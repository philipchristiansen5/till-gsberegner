import { Component } from '@angular/core';
import {Http, HTTP_PROVIDERS} from '@angular/http';
import { TopMenu } from './app.topmenu';

@Component({
  selector: 'my-app',
  template: `
  <top-menu></top-menu>    
  <div class="panel panel-default">    
    <div class="panel-heading">Uge 1</div>
    <div class="panel-body">
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Day</th>
        <th>Start</th>
        <th>End</th>
      </tr>
    </thead>
    
    <tbody>
      <tr *ngFor="let name of names">
        <td>{{name}}</td>
        <td>09:00</td>
        <td>16:00</td>
      </tr>      
    </tbody>
  </table>  
    
  </div>

    
    
</div>
  `,
  directives:[TopMenu]  
})
export class AppComponent {
  private myName = "Jens"    
  private names=["Monday","Thuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
  constructor(private http:Http){        
  }
  
  clicked(){
    this.http.get('http://localhost:8080/greeting')
    .subscribe(response => {      
      this.myName=response.text();                   
    }
    );
    
    this.http.get('http://localhost:8080/names')
    .subscribe(response => {
      this.names=response.json();   
    }
    );
  }  
}
