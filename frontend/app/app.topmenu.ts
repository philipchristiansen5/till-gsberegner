import { Component } from '@angular/core';

@Component({
  selector: 'top-menu',
  template: `
  <nav class="navbar navbar-default">
  
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Tillægsberegner</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>      
      <li><a href="#">Kontakt</a></li> 
    </ul>
  </div>
</nav>
  `
})

export class TopMenu {
}