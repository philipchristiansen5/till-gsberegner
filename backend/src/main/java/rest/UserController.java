package rest;

import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UserController {


    @RequestMapping("/greeting")
    public String greeting() {
        return "Hello World!!!!";
    }

    @RequestMapping("/names")
    public List<String> getNames() {
        List<String> names = new ArrayList<>();
        names.add("Philip");
        names.add("Jens");
        names.add("Maria");
        names.add("Jeanette");
        return names;
    }

    //localhost:8080/user/123
    //PathVariable
    //RequestParameters
    //RequestBody
    @RequestMapping(value="/user/{theId}",method= RequestMethod.GET)
    public int getUser(@PathVariable("theId") int id) {
        return id*2;
    }


}

